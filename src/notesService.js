/* eslint-disable max-len */
/* eslint-disable consistent-return */
const { Note } = require('./models/Notes');
const { getUserId } = require('./utils/getUserId');

const createNote = (req, res) => {
  if (!req.body) return res.status(400).json({ message: 'Note text is required' });
  const { text } = req.body;
  const token = req.headers.authorization.split(' ')[1];
  const uID = getUserId(token);
  if (typeof uID !== 'string') return res.status(400).json({ message: 'User not authorized' });
  const date = new Date();
  try {
    const note = new Note({
      text,
      userId: uID,
      createdDate: date.toISOString(),
    });
    return note.save().then(() => res.status(200).json({ message: 'Success' }));
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

const getNote = (req, res) => {
  if (!req.params.id) return res.status(400).json({ message: 'Note id is required' });
  try {
    Note.findById(req.params.id)
      .then((note) => res.status(200).json({ note }));
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

const updateNote = async (req, res) => {
  if (!req.body) res.status(400).json({ message: 'More information is required' });
  try {
    const note = await Note.findById(req.params.id);
    const { text } = req.body;

    if (text) note.text = text;

    return note.save().then((saved) => res.status(200).json({ message: 'Success', saved }));
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

const deleteNote = (req, res) => {
  if (!req.params.id) res.status(400).json({ message: 'Note id is required' });
  try {
    Note.findByIdAndDelete(req.params.id)
      .then(() => res.status(200).json({ message: 'Success' }));
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

const getMyNotes = (req, res) => {
  try {
    Note.find({ userId: req.user.userId }, '-__v').then((result) => {
      res.json({ notes: result });
    });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

const updateNoteStatusById = (req, res) => {
  if (!req.params) res.status(400).json({ message: 'Note id is required' });
  try {
    Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, [{ $set: { completed: { $eq: [false, '$completed'] } } }])
      .then((result) => {
        res.status(200).json({ message: 'Note was marked as completed', result });
      });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

module.exports = {
  createNote,
  getNote,
  updateNote,
  deleteNote,
  getMyNotes,
  updateNoteStatusById,
};
