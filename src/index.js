const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

const PORT = 8080;
const app = express();

const { usersRouter } = require('./usersRouter');
const { notesRouter } = require('./notesRouter');
const { authRouter } = require('./authRouter');

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_DB_URL);
    app.listen(PORT, () => console.log(`SERVER STARTED ON PORT ${PORT}`));
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
