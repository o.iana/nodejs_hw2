const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

const registerUser = async (req, res) => {
  try {
    const { username, password } = req.body;
    if (!username || !password) return res.status(400).json({ message: 'username and password are required' });
    const date = new Date();
    const user = new User({
      username,
      password: await bcryptjs.hash(password, 10),
      createdDate: date.toISOString(),
    });

    await user.save();
    return res.status(200).json({ message: 'Success' });
  } catch (error) {
    return res.status(500).json({ message: 'Error occurred', error });
  }
};

const loginUser = async (req, res) => {
  if (!req.body.username || !req.body.password) {
    return res.status(400).json({ message: 'Username and Password are required' });
  }
  try {
    const user = await User.findOne({ username: req.body.username });
    if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
      // eslint-disable-next-line no-underscore-dangle
      const payload = { username: user.username, userId: user._id };
      const jwtToken = jwt.sign(payload, process.env.JWT_SECRET);
      return res.status(200).json({ message: 'Success', jwt_token: jwtToken });
    }
    return res.status(400).json({ message: 'User is not authorized' });
  } catch (error) {
    return res.status(500).json({ message: 'Error occurred', error });
  }
};

module.exports = {
  registerUser,
  loginUser,
};
