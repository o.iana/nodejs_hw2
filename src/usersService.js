/* eslint-disable consistent-return */
const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
const { User } = require('./models/Users');
const { getUserId } = require('./utils/getUserId');

const getUserInfo = async (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  const decodedData = jwt.verify(token, process.env.JWT_SECRET);
  if (!decodedData.username) return res.status(400).json({ message: 'User not found' });
  try {
    await User.findOne({ username: decodedData.username });
    const userInfo = {
      // eslint-disable-next-line no-underscore-dangle
      _id: decodedData.userId,
      username: decodedData.username,
      createdDate: decodedData.createdDate,
    };
    return res.status(200).json({ user: userInfo });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

const updateUserPassword = async (req, res) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const id = getUserId(token);
    const newHashPass = await bcryptjs.hash(req.body.newPassword, 10);
    User.findByIdAndUpdate({ _id: id }, { $set: { password: newHashPass } })
      .then(() => {
        res.json({ message: 'Success' });
      });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

const deleteUser = (req, res) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const id = getUserId(token);
    User.findByIdAndDelete({ _id: id })
      .then(() => res.status(200).json({ message: 'Success' }));
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

module.exports = {
  getUserInfo,
  updateUserPassword,
  deleteUser,
};
