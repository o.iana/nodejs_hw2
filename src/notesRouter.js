const express = require('express');

const router = express.Router();
const {
  createNote, getNote, deleteNote, updateNote, getMyNotes,
  updateNoteStatusById,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getMyNotes);

router.get('/:id', authMiddleware, getNote);

router.patch('/:id', authMiddleware, updateNoteStatusById);

router.put('/:id', authMiddleware, updateNote);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
