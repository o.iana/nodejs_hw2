const express = require('express');

const router = express.Router();
const { getUserInfo, updateUserPassword, deleteUser } = require('./usersService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/me', authMiddleware, getUserInfo);

router.patch('/me', authMiddleware, updateUserPassword);

router.delete('/me', authMiddleware, deleteUser);

module.exports = {
  usersRouter: router,
};
